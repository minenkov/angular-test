

export class GiftReason {

  constructor(
    public Id?:number,
    public Code?: string,
    public Description?: string
  ){}

}
