import {Injectable} from '@angular/core';
import {UserModel} from '../models/user.model';
import {environment} from '../../../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {

  private isAuthenticate = false;
  private domain = environment.api;

  constructor(
    private jwtHelperSevice: JwtHelperService,
    private httpClient: HttpClient
  ){}

  login(user: UserModel):Observable<any> {
    return this.httpClient.post(`${this.domain}/auth`,user, {observe:'response'});
  }

  getStore():Observable<any> {
    return this.httpClient.get(`${this.domain}/stores`);
  }

  isAuthenticated(): boolean {
    return this.isAuthenticate;
  }

  setIsAuthenticated(isAuthenticated: boolean){
    this.isAuthenticate=isAuthenticated;
  }

  logout(): boolean{
    localStorage.clear();
    this.isAuthenticate=false;
    return true;
  }

  userName(): string{
    const tok=this.jwtHelperSevice.decodeToken(localStorage.getItem('access_token').slice(7));
    return tok.name;
  }

}
