import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SystemComponent} from './system.component';
import {SystemRoutingModule} from './system-routing.module';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import {FormsModule} from '@angular/forms';
import {BodyService} from './body/body.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../shared/services/token.interceptor';

@NgModule({
  imports: [
    CommonModule,
    SystemRoutingModule,
    FormsModule
  ],
  declarations: [
    SystemComponent,
    HeaderComponent,
    BodyComponent
  ],
  providers:[
    BodyService
  ]
})
export class SystemModule {

}
