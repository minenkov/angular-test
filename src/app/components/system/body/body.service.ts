import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Headers} from '@angular/http';
import {Product} from '../../shared/models/product.model';

@Injectable()
export class BodyService{

  private domain:string = environment.api;

  constructor(
    private httpClient: HttpClient
  ){}


  getProducts(): Observable<any>{
    return this.httpClient.get(`${this.domain}/api/products`);
  }

  save(product: {sourceProduct:number, replacementProduct:number, sourceProductQuantity:number, replacementProductQuantity:number, dateReplacement: string}): Observable<any>{
    return this.httpClient.post(`${this.domain}/api/save`,product,{observe:'response'});
  }

  getGiftReason(): Observable<any>{
    return this.httpClient.get(`${this.domain}/api/giftReason`);
  }

  saveGift(product: {productId:number, productQuantity:number, giftReasonId:number, dateGift: string}): Observable<any>{
    return this.httpClient.post(`${this.domain}/api/save-gift`,product,{observe:'response'});
  }

}
