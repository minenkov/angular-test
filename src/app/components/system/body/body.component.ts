import {Component, EventEmitter, OnInit} from '@angular/core';
import {BodyService} from './body.service';
import {log} from 'util';
import {Product} from '../../shared/models/product.model';
import {GiftReason} from '../../shared/models/giftReason.model';

@Component({
  selector: 'iqf-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent{

  public selectedForBack:number = 0;
  public countForBack:number = 0;
  public selectedBacked:number = 0;
  public countBacked:number = 0;
  public giftCount:number= 0;
  public products:Product[];
  public giftReasons: GiftReason[];
  public giftReasonSelected: number = 0;
  public giftSelected: number = 0;
  public dateReplacement: string;
  public dateGift: string;

  constructor(
    private bodyService: BodyService
  ) {

    this.bodyService.getProducts().subscribe(res => {
      this.products = res;
    });

    this.bodyService.getGiftReason().subscribe(res => {
      this.giftReasons = res;
    });
  }

  send(){
    this.bodyService.save({
      sourceProduct:this.selectedForBack, replacementProduct:this.selectedBacked,
      sourceProductQuantity:this.countForBack, replacementProductQuantity:this.countBacked,
      dateReplacement: this.dateTransform(this.dateReplacement)
    }).subscribe(res=>{
      this.clean();
      alert('Сохранено');
    });
  }

  dateTransform(dateStr:string): string{
    let date: Date=new Date(dateStr);
    let day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
    let month = (date.getMonth()+1) < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
    return day+'-'+month+'-'+date.getFullYear();
  }

  clean(){
    this.selectedForBack = 0;
    this.countForBack = 0;
    this.selectedBacked = 0;
    this.countBacked= 0;
    this.giftCount= 0;
    this.giftReasonSelected= 0;
    this.giftSelected= 0;
    this.dateReplacement = null;
  }

  onChange(id: number){
    this.selectedBacked=id;
  }

  onChangeInput(event){
    if (this.onChangeInputNumber(event)) {
      this.countForBack=0;
    }else this.countBacked=this.countForBack;
  }

  onChangeInputGiftCount(event){
    if (this.onChangeInputNumber(event)) {
      this.giftCount=0;
    }
  }

  onChangeInputNumber(event):boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 95 || charCode > 106)) {
      return true;
    }else return false;
  }

  checkInput(): boolean{
    return this.selectedForBack < 1 || this.countForBack < 1 || this.selectedBacked < 1 || this.countBacked < 1 || this.dateReplacement == null;
  }

  checkInputGift(): boolean{
    return this.giftReasonSelected < 1 || this.giftCount < 1 || this.giftSelected < 1 || this.dateGift == null;
  }

  sendGift(){
    this.bodyService.saveGift({
      productId:this.giftSelected, productQuantity:this.giftCount, giftReasonId:this.giftReasonSelected, dateGift: this.dateTransform(this.dateGift)
    }).subscribe(res=>{
      this.clean();
      alert('Сохранено');
    });
  }

}
