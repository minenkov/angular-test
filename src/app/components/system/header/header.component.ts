import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'iqf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  public userName; string;

  constructor(
    private authService: AuthService
  ) {

    this.userName = this.authService.userName();

  }

  logout(){
    this.authService.logout();
  }

}
