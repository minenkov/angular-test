import {Component} from '@angular/core';
import {UserModel} from '../shared/models/user.model';
import {Router} from '@angular/router';
import {AuthService} from '../shared/services/auth.service';
import {Store} from '../shared/models/store.model';


@Component({
  selector: 'iqf-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {

  public user: UserModel;
  public stores: Store[];

  constructor(
    private router: Router,
    private authService: AuthService
  ){
    this.user= new UserModel();
    this.user.store = 0;
    this.authService.getStore().subscribe(res => {
      this.stores=res;
    });
  }

  onSubmit(){

    this.authService.login(this.user).subscribe(res=>{
      let token = res.headers.get('Authorization');
      if(token != null){
        localStorage.setItem('access_token',token);
        this.authService.setIsAuthenticated(true);
        this.router.navigate(['/system']);
      }
    },error =>{
      alert("ошибка авторизации");
    } );
  }

  checkLogin(): boolean{
    return this.user.name == null || this.user.name.length < 1 || this.user.password == null || this.user.password.length < 1 || this.user.store < 1;
  }

}
