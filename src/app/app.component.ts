import { Component } from '@angular/core';

@Component({
  selector: 'iqf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
